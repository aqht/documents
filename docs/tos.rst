Điều khoản dịch vụ cho ZAQHT #Anh Quang.,JSC
=======================================

***XIN HÃY ĐỌC KỸ CÀNG NHỮNG ĐIỀU KHOẢN VÀ ĐIỀU KIỆN NÀY***

**HỢP ĐỒNG ĐIỀU KHOẢN**

Các Điều khoản và Điều kiện này cấu thành một thỏa thuận ràng buộc về
mặt pháp lý được thực hiện giữa người dùng, cho dù là cá nhân hay người
đại diện (người dùng) và AQHT (chúng tôi) có liên quan đến việc người
dùng truy cập và sử dụng ứng dụng AQHT cũng như bất kỳ hình thức truyền
thông, kênh truyền thông, trang web di động hoặc ứng dụng di động nào,
được liên kết hoặc kết nối với nhau (gọi chung là AQHT,). Người dùng
đồng ý rằng bằng cách truy cập AQHT, bạn đã đọc, hiểu và đồng ý bị ràng
buộc bởi tất cả các Điều khoản và Điều kiện sử dụng này. NẾU BẠN KHÔNG
ĐỒNG Ý VỚI TẤT CẢ CÁC ĐIỀU KHOẢN VÀ ĐIỀU KIỆN, THÌ BẠN ĐÃ THỰC HIỆN TỪ
KHI SỬ DỤNG ỨNG DỤNG VÀ BẠN PHẢI TỪ CHỐI SỬ DỤNG NGAY LẬP TỨC.

Các điều khoản và điều kiện bổ sung hoặc tài liệu có thể được đăng trên
AQHT theo thời gian rõ ràng được kết hợp ở đây bằng cách tham chiếu.
Chúng tôi bảo lưu quyền, theo quyết định riêng của chúng tôi, để thực
hiện các thay đổi hoặc sửa đổi các Điều khoản và Điều kiện này bất cứ
lúc nào và vì bất kỳ lý do gì. Chúng tôi sẽ thông báo cho bạn về bất kỳ
thay đổi nào bằng cách thông báo đến bạn cùng của các Điều khoản và Điều
kiện này và bạn có quyền xem, nhận thông báo cụ thể về từng thay đổi đó.
Bạn có trách nhiệm định kỳ xem lại các Điều khoản và Điều kiện này, đồng
thời bạn sẽ phải tuân theo và hiểu là đã được biết và chấp nhận, những
thay đổi trong mọi Điều khoản và Điều kiện được sửa đổi bằng cách bạn
tiếp tục sử dụng AQHT sau ngày Điều khoản được sửa đổi đó được đăng.

Thông tin được cung cấp trên AQHT không nhằm mục đích phân phối hoặc sử
dụng bởi bất kỳ cá nhân hoặc tổ chức nào trong bất kỳ khu vực hoặc quốc
gia nơi phân phối hoặc sử dụng đó sẽ trái với luật pháp hoặc quy định
hoặc sẽ khiến chúng tôi phải tuân theo bất kỳ yêu cầu đăng ký nào trong
phạm vi quyền hạn đó hoặc Quốc gia. Theo đó, những người chọn truy cập
AQHT từ các địa điểm khác sẽ tự mình làm điều đó và tự chịu trách nhiệm
tuân thủ luật pháp địa phương, nếu và trong phạm vi luật pháp địa phương
được áp dụng.

**QUYỀN SỞ HỮU TRÍ TUỆ**

Trừ khi có quy định khác, AQHT là tài sản độc quyền của chúng tôi và tất
cả mã nguồn, cơ sở dữ liệu, chức năng, phần mềm, thiết kế trang web, âm
thanh, video, văn bản, hình ảnh và đồ họa trên AQHT (gọi chung là
Content) và các thương hiệu, dịch vụ nhãn hiệu và logo có trong đó được
sở hữu hoặc kiểm soát bởi chúng tôi hoặc được cấp phép cho chúng tôi và
được bảo vệ bởi luật bản quyền và nhãn hiệu và các quyền sở hữu trí tuệ
khác và luật cạnh tranh không lành mạnh của Vương quốc Anh , các khu vực
pháp lý nước ngoài và các hội nghị quốc tế. Nội dung và Nhãn hiệu được
cung cấp trên AQHT chỉ được dùng cho mục đích thông tin và sử dụng cá
nhân của bạn. Trừ những điều được quy định rõ ràng trong các Điều khoản
sử dụng này, không có phần nào của AQHT được sao chép, sao chép, tổng
hợp, tái bản, tải lên, đăng tải, hiển thị công khai, mã hóa, dịch,
truyền, phân phối, bán, cấp phép hoặc mặt khác được khai thác với bất kỳ
mục đích thương mại nào mà không có sự cho phép bằng văn bản của chúng
tôi.

**ĐẠI DIỆN NGƯỜI DÙNG**

Bằng cách sử dụng AQHT , bạn tuyên bố và bảo đảm rằng: (1) tất cả thông
tin đăng ký bạn gửi sẽ là đúng, chính xác, hiện hành và đầy đủ; (2) bạn
sẽ duy trì tính chính xác của thông tin đó và cập nhật kịp thời thông
tin đăng ký đó khi cần thiết;] (3) bạn có năng lực pháp lý và bạn đồng ý
tuân thủ các Điều khoản sử dụng này; (4) bạn không dưới 18 tuổi; (5)
không phải là trẻ vị thành niên trong khu vực tài phán mà bạn cư trú
[hoặc nếu là trẻ vị thành niên, bạn đã nhận được sự cho phép của cha mẹ
để sử dụng AQHT; (6) bạn sẽ không truy cập AQHT thông qua các phương
tiện tự động hoặc không phải con người, cho dù thông qua bot, tập lệnh
hay cách khác; (7) bạn sẽ không sử dụng AQHT cho bất kỳ mục đích bất hợp
pháp hoặc trái phép nào; (8) việc sử dụng AQHT của bạn sẽ không vi phạm
bất kỳ luật hoặc quy định hiện hành nào.

Nếu bạn cung cấp bất kỳ thông tin nào không đúng sự thật, không chính
xác, không hiện tại hoặc không đầy đủ, chúng tôi có quyền đình chỉ hoặc
chấm dứt tài khoản của bạn và từ chối mọi hoạt động sử dụng AQHT hiện
tại hoặc trong tương lai (hoặc bất kỳ phần nào trong đó).

**HOẠT ĐỘNG DỰ ÁN**

Bạn không được truy cập hoặc sử dụng AQHT cho bất kỳ mục đích nào khác
ngoài mục đích chúng tôi cung cấp.

**Là người dùng của AQHT , bạn đồng ý không:**

1.  Lấy dữ liệu một cách có hệ thống hoặc nội dung khác từ AQHT để tạo
    hoặc biên dịch, trực tiếp hoặc gián tiếp, một bộ sưu tập, biên dịch,
    cơ sở dữ liệu hoặc thư mục mà không có sự cho phép bằng văn bản của
    chúng tôi.

2.  Thực hiện bất kỳ việc sử dụng trái phép nào của AQHT , bao gồm thu
    thập tên người dùng và / hoặc địa chỉ email của người dùng bằng
    phương tiện điện tử hoặc phương tiện khác cho mục đích gửi email
    không được yêu cầu hoặc tạo tài khoản người dùng bằng phương tiện tự
    động hoặc giả mạo.

3.  Phá vỡ, vô hiệu hóa hoặc can thiệp vào các tính năng liên quan đến
    bảo mật của AQHT , bao gồm các tính năng ngăn chặn hoặc hạn chế sử
    dụng hoặc sao chép bất kỳ Nội dung nào hoặc thực thi các hạn chế đối
    với việc sử dụng AQHT và / hoặc Nội dung có trong đó.

4.  Tham gia vào việc đóng khung trái phép hoặc liên kết với AQHT .

5.  Lừa, lừa gạt hoặc đánh lừa chúng tôi và những người dùng khác, đặc
    biệt là trong mọi nỗ lực tìm hiểu thông tin tài khoản nhạy cảm như
    mật khẩu người dùng;

6.  Sử dụng không đúng cách các dịch vụ hỗ trợ của chúng tôi hoặc gửi
    báo cáo sai về lạm dụng hoặc hành vi sai trái.

7.  Tham gia vào bất kỳ việc sử dụng tự động nào của hệ thống, chẳng hạn
    như sử dụng tập lệnh để gửi nhận xét hoặc tin nhắn hoặc sử dụng bất
    kỳ công cụ khai thác dữ liệu, rô bốt hoặc các công cụ thu thập và
    trích xuất dữ liệu tương tự.

8.  Can thiệp, phá vỡ hoặc tạo gánh nặng không đáng có trên AQHT hoặc
    các mạng hoặc dịch vụ được kết nối với AQHT .

9.  Cố gắng mạo danh người dùng hoặc người khác hoặc sử dụng tên người
    dùng của người dùng khác.

10. Bán hoặc chuyển hồ sơ của bạn.

11. Sử dụng bất kỳ thông tin nào có được từ AQHT để quấy rối, lạm dụng
    hoặc làm hại người khác.

12. Sử dụng AQHT để cạnh tranh với chúng tôi hoặc sử dụng AQHT với Nội
    dung nhằm mục đích nỗ lực tạo doanh thu hoặc doanh nghiệp thương mại
    nào.

13. Giải mã, dịch ngược, phân tách hoặc thiết kế ngược bất kỳ phần mềm
    nào bao gồm hoặc bằng bất kỳ cách nào tạo nên một phần của AQHT.

14. Quấy rối, gây phiền nhiễu, đe dọa hoặc đe dọa bất kỳ nhân viên hoặc
    đại lý nào của chúng tôi tham gia cung cấp bất kỳ phần nào của AQHT
    cho bạn.

15. Xóa bản quyền hoặc thông báo quyền sở hữu khác khỏi mọi nền tảng.

16. Tải lên hoặc truyền (hoặc cố tải lên hoặc để truyền) virus, trojan,
    hoặc vật liệu khác, trong đó có sử dụng quá nhiều chữ in hoa và gửi
    thư rác (đăng tải liên tục của văn bản lặp đi lặp lại), cản trở đó
    với việc sử dụng không bị gián đoạn bất kỳ của đảng và hưởng thụ của
    AQHT hoặc sửa đổi, làm suy yếu, phá vỡ, thay đổi hoặc can thiệp vào
    việc sử dụng, tính năng, chức năng, vận hành hoặc bảo trì của AQHT .

**BÊN THỨ BA**

AQHT có thể chứa (hoặc bạn có thể được gửi qua AQHT) các liên kết đến
các trang web khác ("Bên thứ ba ") cũng như các bài viết, hình ảnh, văn
bản, đồ họa, hình ảnh, thiết kế, âm nhạc, âm thanh, video, thông tin,
ứng dụng, phần mềm và các nội dung hoặc vật phẩm khác thuộc hoặc có
nguồn gốc từ bên thứ ba (" Nội dung thứ ba"). Nội dung của bên thứ ba và
bên thứ ba không thuộc chúng tôi điều tra, theo dõi hoặc kiểm tra tính
chính xác, tính chính xác hoặc tính đầy đủ. Nếu bạn cài đặt hoặc tải
xuống các dịch vụ, nguồn cấp dữ liệu và / hoặc nội dung của bên thứ ba,
bạn phải tuân theo các điều khoản dịch vụ và chính sách quyền riêng tư
của bên thứ ba đó có thể được truy cập thông qua trang web của họ .

**CHÍNH SÁCH BẢO MẬT**

Bạn đồng ý với các điều khoản trong Chính sách bảo mật của chúng tôi về
AQHT và các dịch vụ, trang web và tính năng liên quan của nó. Chính sách
bảo mật của chúng tôi mô tả các thông lệ liên quan đến thông tin được xử
lý bởi AQHT và các công ty liên kết.

**HẠN VÀ KẾT THÚC**

KHÔNG GIỚI HẠN BẤT BẤT KỲ QUY ĐỊNH NÀO KHÁC CỦA CÁC ĐIỀU KHOẢN SỬ DỤNG
NÀY, CHÚNG TÔI ĐẢM BẢO QUYỀN ĐỐI VỚI VIỆC GIẢI QUYẾT DUY NHẤT CỦA CHÚNG
TÔI VÀ KHÔNG CẦN THÔNG BÁO HOẶC TRÁCH NHIỆM PHÁP LÝ CỦA CHÚNG TÔI KHÔNG
CÓ LÝ DO, BAO GỒM MÀ KHÔNG GIỚI HẠN ĐỐI VỚI BẤT KỲ ĐẠI DIỆN, BẢO HÀNH
NÀO, HOẶC COVENANT LIÊN QUAN ĐẾN CÁC ĐIỀU KHOẢN SỬ DỤNG HOẶC ÁP DỤNG BẤT
KỲ LUẬT HOẶC QUY ĐỊNH NÀO

**TUYÊN BỐ TỪ CHỐI**

CHÚNG TÔI TỪ CHỐI BẢO HÀNH VÀ KHÔNG ĐẢM BẢO HOẶC ĐẠI DIỆN VỀ CHÍNH XÁC
HOẶC HOÀN THÀNH NỘI DUNG AQHT VÀ TẤT CẢ THÔNG TIN, NỘI DUNG, VẬT LIỆU VÀ
PHẦN MỀM ĐƯỢC CUNG CẤP CHO BẠN

**GIỚI HẠN TRÁCH NHIỆM**

AQHT VÀ CÁC NỀN TẢNG ỨNG DỤNG BÊN NGOÀI KHÔNG CHỊU TRÁCH NHIỆM PHÁP LÝ
ĐỐI VỚI MỌI THỨ HOẶC DỄ DÀNG ĐỂ SỞ HỮU, MẤT TÀI KHOẢN, MẤT LÃI SUẤT.

**SỰ BỒI THƯỜNG**

Bạn đồng ý bảo vệ, bảo mật và giữ kín thông tin về chúng tôi, bao gồm
các công ty con, chi nhánh và tất cả các cán bộ, đại lý, đối tác và nhân
viên của chúng tôi. Từ chối và chống lại mọi mất mát, thiệt hại, trách
nhiệm, yêu cầu, kể cả luật sư hợp lý được thực hiện bởi bất kỳ bên thứ
ba.

**LUẬT CHI PHỐI**

Các Điều khoản sử dụng và việc sử dụng AQHT của bạn được điều chỉnh và
hiểu theo luật pháp của Vương quốc Anh, áp dụng cho các thỏa thuận được
đưa ra và được thực hiện mà không liên quan đến xung đột của các nguyên
tắc luật pháp.
